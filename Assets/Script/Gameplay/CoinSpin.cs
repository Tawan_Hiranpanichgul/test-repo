using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.ProjectBall.Gameplay
{
    public class CoinSpin : MonoBehaviour
    {
        private const float speed = 2.0f;
        private Vector3 newRotation = Vector3.zero;

        private void Start()
        {
            newRotation.z = speed;
        }

        void Update()
        {
            transform.Rotate(newRotation);
        }
    }
}