#define DEBUG
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TrueAxion.ProjectBall.Gameplay
{
    
    public class PlayerControl : MonoBehaviour
    {
        //PlayerControl
        private Rigidbody rb;
        private float horizontalInput;
        private float verticalInput;
        private const string horizon = "Horizontal";
        private const string vertical = "Vertical";
        private float speed = 10f;
        
        //Score
        private const int maxScore = 3;
        private int score = 0;
        private const string coinTag= "Coin";

        //Text ScoreBoard
        [SerializeField] private Text scoreBoard;
        [SerializeField] private Text winText;

        void Start()
        {
            rb = GetComponent<Rigidbody>();
        }
        
        void Update()
        {
            horizontalInput = Input.GetAxis(horizon);
            verticalInput = Input.GetAxis(vertical);
        }

        private void FixedUpdate()
        {
            rb.AddForce(new Vector3(horizontalInput, 0.0f, verticalInput) * speed);
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag.Contains(coinTag));
            {
                other.gameObject.SetActive(false);
                score += 1;
                scoreBoard.text = "Score : " + score;
#if DEBUG
                Debug.Log("Score : " + score);
#endif
                
                if (score == maxScore)
                {
                    winText.gameObject.SetActive(true);
                    scoreBoard.gameObject.SetActive(false);
                }
            }
        }
    }
}


    

