using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrueAxion.ProjectBall.Gameplay
{
    public class GameCamera : MonoBehaviour
    {
        [SerializeField] private Transform cameraFollow;
        
        private Vector3 cameraOffset;
        private const float smooth = 1f;
        
        void Start()
        {
            cameraOffset = transform.position - cameraFollow.position;
        }
        
        void Update()
        {
            Vector3 newPos = cameraFollow.position + cameraOffset;
            transform.position = Vector3.Slerp(transform.position, newPos, smooth);
        }
    }
}
